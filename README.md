# README #

This is a website for a Hotel. It helps user can view basic information about this HOtel and if suitable users can book any rooms at this
Hotel.
This website is for promote a hotel that has lots of service and useful for user.

### What is this repository for? ###


### How do I get set up? ###
* Have to set up laravel 5.4 
* Node version: v8.1.0
* NPM version: 5.0.4
    
    Install node on ubuntu 16.04

        For details look at the installation instructions. 
        First, choose the Node.js version you need and add the sources for it:

        ---------------------------------------------------------------------
        ### for Node.js v4
        curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
        ### OR for Node.js v5
        curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
        ### OR for Node.js v6
        curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
        ### OR for Node.js v7
        curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
        ---------------------------------------------------------------------

        Then install the nodejs package:
        
        sudo apt-get install -y nodejs

    To check the ver sion of node and npm, run this command:

        node -v 

        npm -v
    
    Install node on window

        

* Composer version: 1.4.2

    Download composer

        Run this in your terminal to get the latest Composer version:

        --------------------------------------------------------------------
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
        php composer-setup.php
        php -r "unlink('composer-setup.php');"

        --------------------------------------------------------------------

* PHP >= 5.6.4 and MYSQL 
    
    If you don't have PHP and MYSQL. Just install it

    * Step 1: Install Apache

        ------------------------------------------

        sudo apt-get update
        
        sudo apt-get install apache2

        ------------------------------------------

    * Step 2: Install Mysql

        ------------------------------------------

        Open terminal and type these command

        sudo apt-get install mysql-server libapache2-mod-auth-mysql php5-mysql

        ------------------------------------------

        Mysql will ask for password. So we should activate it with this command

        ------------------------------------------

        sudo mysql_install_db

        ------------------------------------------

        Finish up Mysql set up

        ------------------------------------------

        sudo /usr/bin/mysql_secure_installation

        ------------------------------------------

        The prompt will ask you for your current root password.
        Type it in.
        
        ------------------------------------------

        Enter current password for root (enter for none): 

        OK, successfully used password, moving on...

* Copy the content in .env.example file then create a new file with the name is .env
* Config database name in .env file
    + DB_DATABASE: hotel_project
    + DB_USERNAME: root
    + DB_PASSWORD: Depend on your evironment. For me is "root"
* To get vendor folder. On project folder, run this command: "composer install"
* To get node_modules folder. On project folder, run this command: "npm install"
* Generate the key of project by using command line: php artisan key:generate
* On project folder. Set up the Laravel collective form by running this command: "composer require "laravelcollective/html":"^5.4.0""
* Run command: "php artisan migrate" to migrate the table into database
* On project folder. You have to type "npm run dev" to build all files on resources folder into public folder that makes the project can work correctly.
* Direct to correct folder of project and type command line: "php artisan serve" to run

### Contribution guidelines ###

* Open browser: type 127.0.0.1:8000 or localhost:8000 to launch the project
