@extends('layouts.theme')
@section('content')
    
<table class="table table-striped custab">
<thead>
    <tr>
        <th>ID</th>
        <th>Room name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th class="text-center">Action</th>
    </tr>
</thead>
        @foreach ($cartItems as $cartItem)
        <tr>
            <td>{{ $cartItem->id }}</td>
            <td>{{ $cartItem->name }}</td>
            <td>
                {!! Form::open(['route' => ['cart.update',$cartItem->rowId], 'method' => 'PUT']) !!}
                <input class="inpCart" name="qty" type="text" value="{{ $cartItem->qty }}">
                <span><input type="submit" class="btn btn-default" value="OK"></span>
                {!! Form::close() !!}
            </td>
            <td>{{ $cartItem->price }}</td>
            <td class="text-center">
                <a class='btn btn-info btn-xs' href="{{ url('/booking/create', [$cartItem->id])}}">
                    <span class="glyphicon glyphicon-edit"></span> Book</a> 
                <a class="btn btn-danger btn-xs" name="del" href="{{ Cart::remove($cartItem->rowId) }}">
                    <span class="glyphicon glyphicon-remove"></span> Delete</a>
            </td>
        </tr>
         @endforeach
        
</table>
@endsection