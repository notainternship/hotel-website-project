<!DOCTYPE html>
<html>
<head>
    <title>View rooms</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

</head>
<body>
    @extends('layouts.theme')
    @section('content')
    <div class="col-lg-8">
        <div class="content-room">
            <div class="l-align-center"><h3>Room name: {{ $room->name }}</h3></div>
            <hr>
            <div class="room-content">
                <div class="room-image">
                    <img src="/{{ $room->image }}" alt="">
                    <div class="overlay"><h3>{{ $room->category['name'] }}</h3></div>
                </div>
                <br>
                <table class="table table-border">
                    <tr>
                        <td>Room name</td>
                        <td>{{ $room->name }}</td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>{{ $room->price }}</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>{{ $room->category['name'] }}</td>
                    </tr>
                    <tr>
                        <td>Date available</td>
                        <td>{{ $room->available }}</td>
                    </tr>
                    <tr>
                        <td>Date unavailable</td>
                        <td>{{ $room->unavailable }}</td>
                    </tr>
                    <tr>
                        <td>Amount people</td>
                        <td>{{ $room->number_people }}</td>
                    </tr>
                    <tr>
                        @if($room->available < $dates)
                            <td>Status</td>
                            <td>
                                <div class="warning-message">
                                    <strong>Out of room</strong>
                                </div>
                            </td>
                        @else
                           <td><a href="{{ route('cart.edit', $room->id) }}"><input type="submit" class="btn btn-success" id="addCart" value="Add to wishlist"></a></td>
                            <td><a href="{{ url('/booking/create', [$room->id])}}"><input type="submit" class="btn btn-danger" value="Book now"></a></td>  
                        @endif
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <!--@include('inc.sidebar')-->
        <div class="room-description">
            <h3>ROOM DESCRIPTION</h3>
            <hr>
            <p>{{ $room->description }}</p>
            <hr>
            @include('inc.sidebar')
        </div>
    </div>
@endsection
</body>
</html>