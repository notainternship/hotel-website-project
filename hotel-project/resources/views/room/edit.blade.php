<!DOCTYPE html>
<html>
<head>
    <title>Edit rooms</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <div class="container">
        <h2>Edit {{ $room->name }}</h2>
        <div class="col-md-4 image-room">
            <img src="/{{ $room->image }}" alt="">
        </div>
        <div class="col-md-8">

        {{ Form::model($room, array('route' => array('room.update', $room->id),'files' => true, 'method' => 'PUT')) }}

            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('price', 'Price') }}
                {{ Form::text('price', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('description', 'Description') }}
                {{ Form::text('description', null, array('class' => 'form-control')) }}
            </div>

            <div class="form-group">
                {{ Form::label('status', 'Status') }}
                {{ Form::select('status', array('1' => 'Available', '2' => 'Non available'), array('class' => 'form-control')) }}
            </div>
            
            <div class="form-group">
                {{ Form::label('number_people', 'Amount people') }}
                {{ Form::text('number_people', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('available', 'Date Available') }}
                {{ Form::date('available', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('unavailable', 'Date Unavailable') }}
                {{ Form::date('unavailable', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('image', 'Image') }}
                {{ Form::file('image', null, array('class' => 'form-control')) }}
            </div>
            <?php $categories = DB::table('categories')->pluck('name','id'); ?>
            <div class="form-group">
                {{ Form::label('category_id', 'Category') }}
                {{ Form::select('category_id', $categories, array('class' => 'form-control')) }}
            </div>
            {{ Form::submit('Edit the Room!', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
        </div>
    </div>
</body>
</html>