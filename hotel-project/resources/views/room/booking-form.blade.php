@extends('layouts.theme')
@section('content')

<div>
    {!! Form::open(['route' => 'booking.store', 'method' => 'post']) !!}
    <div class="text-center"><h2>BOOKING FORM</h2></div>
    <div class="well clearfix">
        <fieldset class="well well-form">
            <legend class="well-legend">Booking Details</legend>
            <div class="row">
                <div class="col-xs-4">
                    <label class="control-label" for="booking-date">Check In</label>
                    <div class="form-group input-group">
                        <span class="input-group-addon "><i class="fa fa-calendar"></i></span>
                        <input class="form-control" type="text" name="created_date" id="datefrom" placeholder="Check In" />
                    </div>
                </div>

                <div class="col-xs-4">
                    <label class="control-label" for="booking-date">Check Out</label>
                    <div class="form-group input-group">
                        <span class="input-group-addon "><i class="fa fa-calendar-check-o"></i></span>
                        <input class="form-control" type="text" name="ended_date" id="dateto" placeholder="Check Out" />
                    </div>
                </div>
                <input type="hidden" name="category_id" value="{{ $rooms->category_id }}">
                <input type="hidden" name="amount" value="1">
                <input type="hidden" name="room_id" value="{{ $rooms->id }}">
               <div class="col-xs-2">
                    <label class="control-label" for="form_days">Adults</label>
                    <select class="form-control" id="form_days" name="amount_person">
                        <option selected value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
                <div class="col-xs-2">
                    <label class="control-label" for="form_days">Children</label>
                    <select class="form-control" id="form_days" name="amount_children">
                        <option selected value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>
    <!-- end Row -->
    </fieldset>
    {!! Form::close() !!}
    @if(Session::has('message'))
        <div class="alert alert-info">
            {{ Session::get('message') }}
        </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <br/>
           <input type="submit" class="btn btn-warning" value="Continue">
        </div>
    </div>
<!-- end Row -->

    </div>
<!-- end Well -->

</div>
@endsection