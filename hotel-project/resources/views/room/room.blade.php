@extends('layouts.theme')
@section('content')
<div class="form-search">
    @include('layouts.form')
</div>

<div class="col-md-8">
    @foreach ($categories as $category)
    <div class="heading">
        <h2 class="title-anchor title-heading">{{ $category->name }}</h2>
    </div>
    @endforeach
    @foreach ($rooms as $room)
    <div class="content-room">
        <div class="content-container-anchor overlay-container-anchor">
            <p><a href="{{url('/room',[$room->id])}}"><img style="width: 100%" src="/{{ $room->image }}" alt=""></a></p>
            <p><strong>Price</strong><span>:</span><br>
            <span>- {{ $room->price}}/ single room</span><br>

            <p><strong>Hotel rates include:</strong><br>
            Buffet breakfast, VAT and service charge, a free bottle of water/day, free wifi.&nbsp;</p>

            <p><strong>Terms:&nbsp;</strong><br>
            + For children sharing rooms with parents: &nbsp;<br>
            &nbsp; &nbsp; &nbsp;- Under 6 yrs old : Free of charge and breakfast for at most 2 children/room.<br>
            &nbsp; &nbsp; &nbsp;- From 6 to 11 yrs old : 35.000 VND extra charge<br>
            &nbsp; &nbsp; &nbsp;- Over 12 yrs old : 70.000 VND extra charge<br>
            + Room order-form will be kept until 18:00. If the guests want to check in sooner or later, they have to notify the Reception Desk.&nbsp;<br>
            + Check-out time is 12:00. In case of checking out later:&nbsp;<br>
            &nbsp; &nbsp; &nbsp;- From 12:00 to 14:00 : 30% of room’s charge is added&nbsp;<br>
            &nbsp; &nbsp; &nbsp;- From 14:00 to 18:00 : 50% of room’s charge is added<br>
            &nbsp; &nbsp; &nbsp;- After 18:00 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: 100% of room’s charge is added<br>
            + Check-in time is 14:00. (Guests can check in sooner if rooms are available)</p>

            <p><strong>Terms of payment:</strong><br>
            - Cash or bank transfer&nbsp;<br>
            - Credit cards: Visa, Master, American Expres</p>

            <p>&nbsp;</p>
            <span><a href="{{ route('cart.edit',$room->id) }}"><button class="btn btn-success">Add to wishlist</button></a></span>
            <span><a href="{{url('/room',[$room->id])}}"><button class="btn btn-danger">View more</button></a></span>
        </div>
    </div>
    @endforeach
</div>
<div class="col-md-4">
    @include('inc.sidebar')
</div>

@endsection