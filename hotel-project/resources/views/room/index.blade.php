@extends('layouts.theme')
@section('content')
<div class="text-center"><h2>ROOMS</h2></div>
<div class="row">
    <?php $rooms = DB::table('rooms')->simplePaginate(2); ?>
    @foreach ($rooms as $room)
    <div class="col-md-4">
        <div class="rooms">
            <a href="{{url('/room',[$room->id])}}"><img src="/{{ $room->image }}" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>{{ $room->name }}</p>
                <p>{{ $room->price }} $</p>
                @if ($room->status == 1)
                    <p>STATUS: AVAILABLE</p>
                @elseif($room->status == 2)
                    <p>STATUS: NO AVAILABLE</p>
                @else
                    <div><p>Not found</p></div>
                @endif
                <span><button class="btn-button"><a href="{{url('/room',[$room->id])}}">View more</a></button></span>
            </center>
        </div> 
    </div>
    @endforeach
</div>
    <div class="text-center">
        {{ $rooms->links()}}
    </div>
@endsection