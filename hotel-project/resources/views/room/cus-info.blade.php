@extends('layouts.theme')
@section('content')
 {!! Form::open(['route' => 'customer.store', 'method' => 'post']) !!} 
 @if(Session::has('message'))
        <div class="alert alert-info">
            {{ Session::get('message') }}
        </div>
    @endif
    <fieldset class="well well-form">
        <legend class="well-legend">Customer</legend>
            <div class="row">
                <div class="col-xs-5">
                    <input type="hidden" name="booking_id" value="{{ $booking->id }}">
                    <label class="control-label" for="firstname">First Name</label>
                    <input class="form-control" type="text" name="firstname" id="firstname" placeholder="First Name" required/>
                    <label class="control-label" for="firstname">Last Name</label>
                    <input class="form-control" type="text" name="lastname" id="firstname" placeholder="Last Name" required/>
                    <label class="control-label" for="phoneNumber">Number</label>
                    <div class="form-group input-group">
                        <span class="input-group-addon primary"><i class="fa fa-phone"></i></span>
                        <input type="text" class="form-control" name="phone" id="phoneNumber" placeholder="Phone Number" pattern="[0].{11,}" required>
                    </div>
                    <label class="control-label" for="email">Email Address</label>
                    <div class="form-group input-group">
                        <span class="input-group-addon primary"><i class="fa fa-envelope-o"></i></span>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email address" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                    </div>
                    <label class="control-label" for="email">Address</label>
                    <div class="form-group input-group">
                        <span class="input-group-addon primary"><i class="fa fa-envelope-o"></i></span>
                        <input type="text" class="form-control" name="address" id="email" placeholder="Address" required>
                    </div>
                </div>
                <div class="col-xs-7">
                    <label class="control-label">Message</label>
                    <textarea class="input-xlarge form-control" id="message" name="message" rows="10" required></textarea>
                </div>
            </div>
            <input type="submit" class="btn btn-warning" value="Send">
    <!-- end Row -->
    </fieldset>
     {!! Form::close() !!} 
@endsection