@section('title', '| Add Room')
<div class="row">
    <!-- Modal -->
    
    <div id="add_project" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
        {!! Form::open(['route' => 'room.store',
                            'files' => true,
                            'method' => 'post']) !!}
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header login-header">
                    <h4 class="modal-title">Add Room</h4>
                </div>
                <div class="modal-body">
                    <input type="text" placeholder="Room Name" name="name">
                    <input type="text" placeholder="Price" name="price">
                    <textarea name="description" placeholder="Description"></textarea>
                    {{ Form::label('status', 'Status')}}
                    <select placeholder="Status" name="status">
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                    
                    {{ Form::file('image', array('class' => 'form-control')) }}
                    <?php $categories = DB::table('categories')->pluck('name','id'); ?>
                    {{ Form::label('category_id', 'Category') }}
                    {{ Form::select('category_id', $categories, null, ['class' => 'form-control']) }} 
                </div>
                <div class="modal-footer">
                    <button type="submit" class="cancel">Close</button>
                    <button type="submit" class="add-project">Add</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>