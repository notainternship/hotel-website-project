<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/app.css">
    <title>Luxury Hotel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    
</head>
<body>
    <header>
        @include('inc.header')
    </header>
    <div class="slide">
        @include('inc.slider')
    </div>
    <div class="content">
        @include('layouts.form')
        <div class="text-center">
            <h2>Our Hotel</h2>
            <p>Etiam scelerisque nunc eu sagittis rhoncus. </p>
            <p>Mauris eget ullamcorper tortor. Phasellus nisi enim, mattis eu felis sit amet, malesuada pharetra purus. </p>
            <p>Integer congue dapibus felis, at vulputate quam lobortis ac.</p>
        </div>
        <div class="hotel-list">
            <div class="container">
                <div class="col-sm-3 col-md-3 col-lg-3"><a href="#"><img src="/images/hotel_home_01.jpg" alt="Image"></a></div>
                <div class="col-sm-3 col-md-3 col-lg-3"><a href="#"><img src="/images/hotel_home_02.jpg" alt="Image"></a></div>
                <div class="col-sm-3 col-md-3 col-lg-3"><a href="#"><img src="/images/hotel_home_03.jpg" alt="Image"></a></div>
                <div class="col-sm-3 col-md-3 col-lg-3"><a href="#"><img src="/images/hotel_home_04.jpg" alt="Image"></a></div>
            </div>
        </div>
    </div>
    <div>
        <div class="text-center">
            <h2>ROOM AND SUITES</h2>
        </div>
        <div clas="room-list">
            <div class="container">
            <?php $rooms = DB::table('rooms')->where('category_id','=','2')->get();  ?>
                
                <div class="room-list-content">
                @foreach ($rooms as $room)
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a href="{{ url('/room/'.$room->id) }}"><img class="room-image" src="/{{ $room->image }}" alt=""></a>
                    </div>
                @endforeach
                </div>
            </div>
            <div class="container">
            <?php $roomImages = DB::table('rooms')->where('category_id','=','1')->get(); ?>
                <div class="room-list-content">
                @foreach ($roomImages as $roomImage)
                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <a href="{{ url('/room/'.$roomImage->id) }}"><img class="room-image" src="/{{ $roomImage->image }}" alt=""></a>
                    </div>
                @endforeach
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="container">
        <div class="content-banner">
            <div class="text-center"><h2 class="text-title">GUEST REVIEW</h2></div>
            <div class="col-md-4">
                <article>
                    <div class="content-box">
                        <center><img class="review-image" src="/images/guestA.jpg" alt=""></center>
                        <span>While staying in Indianapolis on my business affairs I always stay at this place. 
                        The price/quality ratio here is just outstanding, 
                        allowing me to save more money for other spends while visiting… My rating is 5 out of 5!</span>
                        <p><strong><i>Kelly Smith</i></strong></p>
                    </div>
                </article>
                
            </div>
            <div class="col-md-4">
                <article>
                    <div class="content-box">
                        <center><img class="review-image" src="/images/guestB.jpg" alt=""></center>
                        <span>Choosing a hotel is always challenging, especially if you’re on a hectic schedule and are visiting the place for the first time. 
                        That’s why I was so happy that my guess with this hotel was definitely a lucky one.</span>
                        <p><strong><i>Taylor Rose</i></strong></p>
                    </div>
                </article>
            </div>
            <div class="col-md-4">
                <article>
                    <div class="content-box">
                        <center><img class="review-image" src="/images/guestC.jpg" alt=""></center>
                        <span>I’ve been visiting Indianapolis last year while touring across the Midwest with my family. The Family room is just great, 
                        the hotel’s restaurant (and bar) are nice and overall, even the price was very reasonable…</span>
                        <p><strong><i>Karen Kent</i></strong></p>
                    </div>
                </article>
            </div>
        </div>
    </div>
    @include('inc.footer')
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/custom.js"></script>
    
</body>
</html>
