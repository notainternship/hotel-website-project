@extends('layouts.theme')
@section('content')
@include('layouts.form')
<div class="text-center"><h2>ROOMS</h2></div>
<div class="row">
@if(isset($details))
    @foreach($details as $room)
    <div class="col-md-4">
        <div class="rooms">
            <a href="{{url('/room',[$room->id])}}"><img src="/{{ $room->image }}" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>{{ $room->name }}</p>
                <p>{{ $room->price }} $</p>
                <p>{{ $room->category_id }}</p>
                @if ($room->status == 1)
                    <p>STATUS: AVAILABLE</p>
                @elseif($room->status == 2)
                    <p>STATUS: NO AVAILABLE</p>
                @else
                    <div><p>Not found</p></div>
                @endif
                <p>Date available: {{ $room->available }}</p>
                <p>Date unavailable: {{ $room->unavailable }}</p>
                <p>Amount people: {{ $room->number_people }}</p>
                <span><button class="btn-button"><a href="{{url('/room',[$room->id])}}">View more</a></button></span>
            </center>
        </div> 
    </div>
    @endforeach
@endif  
</div>
<div class="text-center">
    {{ $details->links()}}
</div>
@endsection