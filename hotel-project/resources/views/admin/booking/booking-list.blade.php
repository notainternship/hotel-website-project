<div class="row">
    <!-- Modal -->
    <?php $lists = DB::table('bookings')->get(); ?>
    <div id="list_booking" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header login-header">
                    <h4 class="modal-title">Booking list</h4>
                </div>
               
                <table class="table table-striped table-bordered">
                    <thread>
                        <tr>
                            <td>ID</td>
                            <td>Amount</td>
                            <td>Amount person</td>
                            <td>Amount Children</td>
                            <td>Type room</td>
                            <td>Room name</td>
                            <td>Create date</td>
                            <td>End date</td>
                        </tr>
                    </thread>
                    <tbody>
                     @foreach ($lists as $list)
                        <tr>
                            <td>{{ $list->id }}</td>
                            <td>{{ $list->amount }}</td>
                            <td>{{ $list->amount_person }}</td>
                            <td>{{ $list->amount_children }}</td>
                            <td>{{ $list->category_id }}</td>
                            <td>{{ $list->room_id }}</td>
                            <td>{{ $list->created_date }}</td>
                            <td>{{ $list->ended_date }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>