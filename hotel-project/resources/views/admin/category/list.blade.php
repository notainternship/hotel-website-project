@section('title', '| Add Category')
<div class="row">
    <!-- Modal -->
    <?php $lists = DB::table('categories')->get(); ?>
    <div id="list_category" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header login-header">
                    <h4 class="modal-title">Category list</h4>
                </div>
                <div class="modal-body">
                    @foreach ($lists as $list)
                    <ul>
                        <li>{{ $list->name }}</li><hr>
                    </ul>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>