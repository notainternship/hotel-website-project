@section('title', '| Add Category')
<div class="row">
    <!-- Modal -->
    
    <div id="add_category" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
        {!! Form::open(['route' => 'category.store']) !!}
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header login-header">
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">
                    <input type="text" placeholder="Category Name" name="name">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="cancel" data-dismiss="modal">Close</button>
                    <button type="submit" class="add-project" data-dismiss="modal">Add</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>