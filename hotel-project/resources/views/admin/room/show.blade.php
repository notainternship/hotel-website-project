<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <style>
        td {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('admin/room') }}">Nerd Alert</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('admin/room') }}">View All Nerds</a></li>
                <li><a href="{{ URL::to('admin/room/create') }}">Create a Nerd</a>
            </ul>
        </nav>

        <?php $rooms = DB::table('rooms')->get(); ?>
        @foreach ($rooms as $room)
        <table class="table table-striped table-bordered">
            <thread>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Price</td>
                    <td>Description</td>
                    <td>Status</td>
                </tr>
            </thread>
            <tbody>
                <tr>
                    <td>{{ $room->id }}</td>
                    <td>{{ $room->name }}</td>
                    <td>{{ $room->price }}</td>
                    <td>{{ $room->description }}</td>
                    <td>{{ $room->status }}</td>
                </tr>
            </tbody>
        </table>
        @endforeach
    </div>
</body>
</html>