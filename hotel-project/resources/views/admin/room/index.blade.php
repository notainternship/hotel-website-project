<style>
    td {
        border: 1px solid black;
        padding: 5px;
    }
</style>
<div class="row">
    <!-- Modal -->
    <div id="index" class="modal fade" role="dialog" style="display: none;">
        <h1>All the Rooms</h1>
    <?php $room = DB::table('rooms')->get(); ?>
        <!-- will be used to show any messages -->
        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        @if (isset($room))
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Price</td>
                    <td>Description</td>
                    <td>Status</td>
                    <td>Amount people</td>
                    <td>Date Available</td>
                    <td>Date Unavailable</td>
                    <td>Image</td>
                    <td>Category</td>
                    <td>More</td>
                </tr>
            </thead>
            <tbody>
            @foreach($room as $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->price }}</td>
                    <td>{{ $value->description }}</td>
                    <td>{{ $value->status }}</td>
                    <td>{{ $value->number_people }}</td>
                    <td>{{ $value->available }}</td>
                    <td>{{ $value->unavailable }}</td>
                    <td>{{ $value->image }}</td>
                    <td>{{ $value->category_id }}</td>

                    <!-- we will also add show, edit, and delete buttons -->
                    <td>

                        <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                        <!-- we will add this later since its a little more complicated than the other two buttons -->
                        {{ Form::open(array('url' => 'room/' . $value->id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Delete this Room', array('class' => 'btn btn-warning')) }}
                        {{ Form::close() }}

                        <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                        <a class="btn btn-small btn-success" href="{{ URL::to('admin/room/' . $value->id) }}">Show this Room</a> |

                        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                        <a class="btn btn-small btn-info" href="{{ URL::to('admin/room/' . $value->id . '/edit') }}">Edit this Room</a> |

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif
    </div>
</div>