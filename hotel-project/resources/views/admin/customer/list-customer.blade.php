<div class="row">
    <!-- Modal -->
    <?php $lists = DB::table('customers')->get(); ?>
    <div id="list_customer" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">
    
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header login-header">
                    <h4 class="modal-title">Booking list</h4>
                </div>
               
                <table class="table table-striped table-bordered">
                    <thread>
                        <tr>
                            <td>First Name</td>
                            <td>Last Name</td>
                            <td>Address</td>
                            <td>Email</td>
                            <td>Phone</td>
                            <td>messages</td>
                            <td>Booking ID</td>
                        </tr>
                    </thread>
                    <tbody>
                     @foreach ($lists as $list)
                        <tr>
                            <td>{{ $list->firstname }}</td>
                            <td>{{ $list->lastname }}</td>
                            <td>{{ $list->address }}</td>
                            <td>{{ $list->email }}</td>
                            <td>{{ $list->phone }}</td>
                            <td>{{ $list->message }}</td>
                            <td>{{ $list->booking_id }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>