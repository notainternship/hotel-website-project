<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Dashboard</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="/css/admin-css.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body class="home">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <div class="container-fluid display-table">
        <div class="row display-table-row">
            <div class="col-md-2 col-sm-1 hidden-xs display-table-cell v-align box" id="navigation">
                <div class="navi">
                    <ul>
                        <li class="active"><a href="/home"><i class="fa fa-home" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Home</span></a></li>
                        <li><a href="#" id="room-index" data-target="#index"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Room</span></a></li>
                        <li><a href="#" id="add" data-target="#add_project"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Add room</span></a></li>
                        <li><a href="#" id="booking"><i class="fa fa-user" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Booking list</span></a></li>
                        <li><a href="#" id="category" data-target="#add_category"><i class="fa fa-calendar" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Add Category</span></a></li>
                        <li><a href="#" id="listcategory" data-target="#list_category"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Category list</span></a></li>
                        <li><a href="#" id="customer" data-target="#list_customer"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Customer</span></a></li>
                        <li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i><span class="hidden-xs hidden-sm">Setting</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10 col-sm-11 display-table-cell v-align">
                <!--<button type="button" class="slide-toggle">Slide Toggle</button> -->
                <header class="header">
                    <div class="row">
                    <div class="col-md-7">
                        <nav class="navbar-default pull-left">
                            <div class="navbar-header" style="display: none;">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </nav>
                    </div>
                    <div class="col-md-5">
                        <div class="header-rightside">
                            <ul class="list-inline header-top pull-right">
                                <li><img src="/images/admin.jpg" alt="admin"></li><br>
                                <li><a href="#">Hi {{ Auth::user()->name }}</a></li>
                                @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                <br>
                                <li class="hidden-xs"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" data-toggle="modal">Log out</a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                @endif
                            </ul>
                        </div>
                    </div>
                </header>
                <div class="user-dashboard">
                    <h3>Welcome to Admin Dashboard</h3>
                    @include('admin.room.create')
                    @include('admin.room.index')
                    @include('admin.category.create')
                    @include('admin.category.list')
                    @include('admin.booking.booking-list')
                    @include('admin.customer.list-customer')
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/custom.js"></script>
</body>
</html>