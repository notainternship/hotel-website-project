<!DOCTYPE html> 
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Luxury Hotel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <header>
        @include('inc.header')
        @include('inc.banner')
    </header>
        
    <div class="container">
    @if(Request::is('/'))
        @include('inc.showcase')
    @endif
        <div class="row">
            <div class="col-md-8 col-lg-8">
                @include('inc.messages')
                @yield('content')
            </div>
            <div class="col-md-4 col-lg-4">
                @if(Request::is('login'))
                    @include('inc.messages')
                @elseif (Request::is('register'))
                    @include('inc.messages')
                @elseif (Request::is('rooms'))
                    @include('inc.messages')
                @else
                    @include('inc.sidebar')
                @endif
            </div>
        </div>
    </div>
    @include('inc.footer')
    <script type="text/javascript" src="/js/custom.js"></script>
</body>
</html>
