@extends('layouts.theme')
@section('content')
<div class="text-center"><h2>ROOMS</h2></div>
<div class="row">
    <?php $rooms = DB::table('rooms')->get(); ?>
@foreach ($rooms as $room)
    <div class="col-md-4">
        <div class="rooms">
            <a href="/room-detail"><img src="/{{ $room->image }}" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>{{ $room->name }}</p>
                <p{{ $room->price }}<sup>$</sup></p>
                @if ($room->status == 1)
                    <p>STATUS: AVAILABLE</p>
                @else
                <p>STATUS: NO AVAILABLE</p>
                @endif
                <span><button><a href="/room-detail">View more</a></button></span>
            </center>
        </div> 
    </div>
@endforeach
    <div class="col-md-4">
        <div class="rooms">
            <a href="/room-detail"><img src="/images/room-2.jpg" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>201B ROOMS</p>
                <p>PRICE: 160$</p>
                <p>STATUS: AVAILABLE</p>
                <span><button><a href="/room-detail">View more</a></button></span>
            </center>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="rooms">
            <a href="/room-detail"><img src="/images/room-3.jpg" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>201B ROOMS</p>
                <p>PRICE: 160$</p>
                <p>STATUS: AVAILABLE</p>
                <span><button><a href="/room-detail">View more</a></button></span>
            </center>
        </div> 
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="rooms">
            <a href="/room-detail"><img src="/images/room-4.jpg" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>201B ROOMS</p>
                <p>PRICE: 160$</p>
                <p>STATUS: AVAILABLE</p>
                <span><button><a href="/room-detail">View more</a></button></span>
            </center>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="rooms">
            <a href="/room-detail"><img src="/images/room-5.jpg" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>201B ROOMS</p>
                <p>PRICE: 160$</p>
                <p>STATUS: AVAILABLE</p>
                <span><button><a href="/room-detail">View more</a></button></span>
            </center>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="rooms">
            <a href="/room-detail"><img src="/images/room-6.jpg" alt=""></a>
        </div>
        <div class="rooms-info">
            <center>
                <p>201B ROOMS</p>
                <p>PRICE: 160$</p>
                <p>STATUS: AVAILABLE</p>
                <span><button><a href="/room-detail">View more</a></button></span>
            </center>
        </div> 
    </div>
</div>
@endsection