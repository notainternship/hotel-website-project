<div class="container">
    <div class="form">
    <?php $categories = DB::table('categories')->get();  ?>
        <form class="form-inline" action="{{ route('search.create') }}" method="get">
            <div class="form-group">
                <select name="roomList" id="seclect" class="form-control">
                    <option>Select room</option>
                    @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="text" id="datefrom" class="form-control" placeholder="Date available" name="dateAvail">
            </div>
            <div class="form-group">
                <input type="text" id="dateto" class="form-control" placeholder="Date unavailable" name="dateUnAvail">
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="number_people" placeholder="Number of people">
            </div> 
            <button type="submit" class="btn btn-default">Search</button>
        </form>
    </div>
</div>