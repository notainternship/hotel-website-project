<!DOCTYPE html>
<html>
<head>
    <title>View rooms</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ URL::to('admin') }}">Back to Admin Dashboard</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="{{ URL::to('admin/room') }}">View All room</a></li>
                <li><a href="{{ URL::to('admin/room/create') }}">Create a Room</a>
            </ul>
        </nav>

        <h2>Showing {{ $room->name }}</h2>
        <div class="col-md-4">
            <img class="room-image" src="/{{ $room->image }}" alt="">
        </div>
        <div class="jumbotron text-center">
            <h2>{{ $room->name }}</h2>
            <p>
                <strong>Price:</strong> {{ $room->price }}<br>
                <strong>Description:</strong> {{ $room->description }}
            </p>
        </div>
    </div>
</body>
</html>