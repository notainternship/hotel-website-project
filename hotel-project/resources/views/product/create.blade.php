@extends('layouts.app')
@section('content')
    <h3>Add product</h3>
    <div class="row">
        {!! Form::open(['route' => 'product.store', 'method' => 'post', 'files' => true]) !!}
            <div class="form-group">
                {{ Form::label('name', 'Name')}}
                {{ Form::text('name', null, array('class' => 'form-control'))}}
            </div>

            <div class="form-group">
                {{ Form::label('description', 'Description')}}
                {{ Form::text('description', null, array('class' => 'form-control'))}}
            </div>

            <div class="form-group">
                {{ Form::label('category_id', 'Category')}}
                {{ Form::select('category_id', $categories,null, array('class' => 'form-control','placeholder' => 'Select Category'))}}
            </div>

            <div class="form-group">
                {{ Form::label('image', 'Images')}}
                {{ Form::file('image', array('class' => 'form-control'))}}
            </div>
            {{Form::submit('Add', array('class' => 'btn btn-danger'))}}
        {!! Form::close() !!}
    </div>
    <br>
@endsection