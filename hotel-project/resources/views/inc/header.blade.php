<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Luxury Hotel</a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
            <div class="header-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/">Home</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Rooms</a>
                        <ul class="dropdown-menu">
                        <?php $categories = DB::table('categories')->get(); ?>
                        @foreach($categories as $category)
                            <li><a href="{{ url('room/room/'.$category->id) }}">{{ $category->name }}</a></li>
                        @endforeach
                        </ul>
                    </li>
                    <li><a href="/contact">Contact</a></li>
                    
                    <li><a href="/cart">
                        <i class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                    Your Wishlist <span>{{ Cart::count()}}</span></a></li>
                </ul>
            </div>
        </div><!--/.nav-collapse -->
    </div>
</nav>