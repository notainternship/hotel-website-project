@section('sidebar')
<div class="sidebar">
    <h3>FACILITIES:</h3>
    
    <table class="table table-hover" style="width: 100%;">
        <tr>
            <td>Twn Room (2 single beds)</td>
        </tr>
        <tr>
            <td>Double Room ( 1 double bed)</td>
        </tr>
        <tr>
            <td>Triple Room (1 double bed + 1single bed or 3 single beds)</td>
        </tr>
        <tr>
            <td>Wifi</td>
        </tr>
        <tr>
            <td>Cable TV</td>
        </tr>
        <tr>
            <td>Air-conditioner</td>
        </tr>
        <tr>
            <td>Dressing-table</td>
        </tr>
        <tr>
            <td>Bath-tub</td>
        </tr>
        <tr>
            <td>Hot and cold water system</td>
        </tr>
        <tr>
            <td>1 bottle of water/day/person</td>
        </tr>
        <tr>
            <td>Telephone</td>
        </tr>

    </table>
</div>
@show