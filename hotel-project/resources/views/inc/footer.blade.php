<div class="sub-footer">
    <div class="container">
        <div class="col-sm-4 col-md-4 col-lg-4">
            <h4 class="f-title"><a href="/"><img class="f-logo" src="/images/logo.png" alt=""></a></h4>
            <p>While the Modern Hotel is neither the oldest nor the biggest one in Indianapolis or Indiana, 
                we've got our own considerably long history of upholding a benchmark level of quality. 
                Our biggest advantage over other hotels in the city is our balanced approach. 
                With just 35 rooms, we're a quiet place to stay at.</p>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
            <h4 class="f-title">POPULAR POSTS</h4>
            <div>
                <span>By <a href="">Tailor</a> - <a href="">Aug. 01</a> - <a href="">3 comments</a></span>
                <p><a class="comment" href="">Mauris eget ullamcorper tortor. 
                    Phasellus nisi enim, mattis eu felis sit amet, malesuada pharetra purus.</a></p>
            </div>
            <div>
                <span>By <a href="">John</a> - <a href="">Aug. 02</a> - <a href="">30 comments</a></span>
                <p><a class="comment" href="">A Definitive Guide to the Best Dining and Drinking Venues in the City.</a></p>
            </div>
            <div>
                <span>By <a href="">Paul</a> - <a href="">Aug. 03</a> - <a href="">13 comments</a></span>
                <p><a class="comment" href="">Mauris eget ullamcorper tortor. 
                    Phasellus nisi enim, mattis eu felis sit amet, malesuada pharetra purus.</a></p>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
            <h4 class="f-title">CONTACTS</h4>
            <ul>
                <li><a href=""><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></a><a href="">410 S Missouri St, Indianapolis, IN 46225, USA</a></li>
                <li><a href=""><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a><a href="">Contact us directly at +3(123) 789-5789</a></li>
                <li><a href=""><span class="glyphicon glyphicon-envelope"></span></a> <a href="mailto:notpn2017@gmail.com">info@demolink.org</a></li>
            </ul>
        </div>
    </div>
</div>
<footer id="footer" class="text-center">
    <div class="footer-corporate__inner">
        <p class="rights">©&nbsp;Modern Hotel&nbsp;<span id="copyright-year">2017</span>. All Right Reserved.
        <span>&nbsp;</span><a class="link-primary" href="">Privacy Policy</a>
            <!-- {%FOOTER_LINK}-->
        </p>
    </div>
</footer>