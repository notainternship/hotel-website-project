<div class="top-banner">
    @if (Request::is('room'))
        <img src="/images/banner_rooms.jpg" alt="">
    @elseif (Request::is('contact'))
        <img src="/images/banner_contact.jpg" alt="">
    @elseif (Request::is('service'))
        <img src="/images/banner_spa.jpg" alt="">
    @elseif (Request::is('room-detail'))
        <img src="/images/banner_room.jpg" alt="">
    @else
        <img src="/images/banner_room.jpg" alt="">
    @endif
</div>