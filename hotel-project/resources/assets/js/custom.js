/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show-dropdown");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show-dropdown')) {
        openDropdown.classList.remove('show-dropdown');
      }
    }
  }
}
$(function () {
  $('#showForm').on('click', function() {
    $('.div_form').toggle();
  });
});
jQuery(document).ready(function($) {
  $("#datefrom").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 });
  $("#dateto").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 });
});
$(document).ready(function(){
   $('[data-toggle="offcanvas"]').click(function(){
       $("#navigation").toggleClass("hidden-xs");
   });
});
$(function () {
  $('#add').on('click', function() {
    $('#add_project').toggle();
    $('#add_project').removeAttr('#index');
  });

  $('#room-index').on('click', function() {
    $('#index').removeAttr('#add_project');
    $('#index').toggle();
  });

  $('#listcategory').on('click', function() {
    $('#list_category').toggle();
  });

  $('#category').on('click', function() {
    $('#add_category').toggle();
  });

  $("#booking").click(function(){
    $('#list_booking').toggle();
  });

  $("#customer").click(function(){
    $('#list_customer').toggle();
  });

  $("#addCart").click(function(){
    alert('Add to cart successfull! ^^');
  });
});

$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(100);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(100);
});