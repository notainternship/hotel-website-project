<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart as Cart;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $categories = DB::table('categories')->get();
        return \View::make('welcome', compact('categories'));
    }
    public function showCategory()
    {
        

        return \View::make('welcome', compact('category'));
    }
}
