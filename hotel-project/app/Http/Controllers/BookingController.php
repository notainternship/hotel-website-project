<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Booking;
use Validator;
use DB;
use Session;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // show the view and pass the  to it
        $rooms = Room::find($id);
        return view('room.booking-form', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $rooms = Room::find($id);
        return \View::make('room.booking-form', compact('booking'),[ 'rooms' => $rooms]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'created_date' => 'required',
            'ended_date' => 'required',
            'category_id' => 'required',
            'amount' => 'required',
            'amount_person' => 'required',
            'amount_children' => 'required'
        ));

        $booking = new Booking;
        $booking->created_date = $request->created_date;
        $booking->ended_date = $request->ended_date;
        $booking->category_id = $request->category_id;
        $booking->amount = $request->amount;
        $booking->amount_person = $request->amount_person;
        $booking->amount_children = $request->amount_children;
        $booking->room_id = $request->room_id;
        $booking->save();

        return redirect('customer/create/'.$booking->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
