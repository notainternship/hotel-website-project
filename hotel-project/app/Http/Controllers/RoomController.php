<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use App\Booking;
use Redirect;
use App\Category;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('room.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('room.booking-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|max:255',
            'price' => 'required',
            'description' => 'required',
            'category_id' => 'required'
        ));

        // store in the database

        $room = new Room;

        $room->name = $request->name;
        $room->price = $request->price;
        $room->description = $request->description;
        $room->status = $request->status;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $image->getClientOriginalName();
            $location = 'image/' . $filename;
            Image::make($image)->resize(800, 400)->save($location);
        }
        
        $room->image = 'image/'.$filename;
        $room->category_id = $request->category_id;
        $room->save();

        return redirect()->route('admin.admin-template');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function adminShow($id)
    {
        // get the 
        $room = Room::find($id);

        // show the view and pass the  to it
        return \View::make('room.adminShow')
            ->with('room', $room);
    }


    public function show($id)
    {
        // get the 
        $room = Room::find($id);
        $dates = date('Y-m-d');
        // show the view and pass the  to it
        return \View::make('room.show', compact('room','dates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the 
        $room = Room::find($id);

        // show the edit form and pass the
        return \View::make('room.edit')
            ->with('room', $room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        
        $this->validate($request, array(
            'name' => 'required|max:255',
            'price' => 'required',
            'description' => 'required',
            'category_id' => 'required'
        ));

            $room = Room::find($id);
            $room->name         = $request->name;
            $room->price        = $request->price;
            $room->description  = $request->description;
            $room->status       = $request->status;
            $room->number_people= $request->number_people;
            $room->available    = $request->available;
            $room->unavailable  = $request->unavailable;
            
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = $image->getClientOriginalName();
                $location = 'image/' . $filename;
                Image::make($image)->resize(800, 400)->save($location);
            }
        
            $room->image = 'image/'.$filename;
            $room->category_id = $request->category_id;
            $room->save();
        
            Session::flash('message', 'Successfully updated room!');
            return Redirect::to('admin')->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $room = Room::find($id);
        $room->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the room!');
        return Redirect::to('admin');
    }
    public function roomCates($id)
    {
        $categories = Category::where('id', '=', $id)->get();
        $rooms = Room::where('category_id','=', $id)->get();

        return view('room.room', compact('rooms', 'categories'));
    }
}
