<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Respone;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CookieController extends Controller
{
    public function setCookie(Request $request)
    {
        $minutes = 1;
        $respone = new Respone('Hello');
        $respone->withCookie(cookie('name', 'not', $minutes));
        return $respone;
    }
    public function getCookie(Request $request)
    {
        $value = $request->cookie('name');
        echo $value;
    }    
}
