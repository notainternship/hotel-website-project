<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Room;
use Illuminate\Database\Query\Builder;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $room = Input::get ('roomList');
        $dateAvail = Input::get ('dateAvail');
        $dateUnAvail = Input::get ('dateUnAvail');
        $people = Input::get ('number_people');
        $room = Room::where('category_id','LIKE','%'.$room.'%')
                    ->orWhere('available','LIKE','%'.$dateAvail.'%')
                    ->orWhere('unavailable','LIKE','%'.$dateUnAvail.'%')
                    ->orWhere('number_people','LIKE','%'.$people.'%')->paginate(6);
        if(count($room) > 0) {
            return view('result')->withDetails($room)->withQuery ($room);
        } else 
            return view ('result')->withMessage('No Details found. Try to search again !');
        // return view('welcome');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
