<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $fillable = ['customer_id','room_id','date_in','date_out','total_charge','notes'];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    public function room()
    {
        return $this->belongsTo('App\Room');
    }
}
