<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['firstname','lastname','address','email','phone','message','booking_id'];

    public function checkout()
    {
        return $this->hasMany('App\Checkout');
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
}
