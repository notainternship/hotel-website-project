<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name','price','description','status','category','image'];
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function checkout()
    {
        return $this->hasMany('App\Checkout');
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
}
