<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function room()
    {
        return $this->hasMany(Room::class);
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
}
