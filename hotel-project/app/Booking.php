<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['amount', 'amount_person', 'amount_children', 'category_id', 'created_date','ended_date'];
    
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function room()
    {
        return $this->belongsTo('App\Room');
    }
    
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
