<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
App::register('Gloudemans\Shoppingcart\ShoppingcartServiceProvider');

Auth::routes();

Route::get('/', 'HomeController@index');

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.admin-template');
    Route::get('/room/create','RoomController@create');
    Route::get('/room/index','RoomController@index');
    Route::resource('/room','RoomController');
});

Route::get('/contact', function () {
    return view('contact');
});
// Route::post('/contact/submit', 'MessagesController@submit');

Route::resource('category','CategoriesController');
Route::get('/admin', 'AdminController@index')->name('admin.admin-template');
// Route::get('/admin/room/create', 'RoomController@create');


Route::prefix('rooms')->group(function (){
    Route::get('/', 'RoomsController@index');
    Route::get('/room-detail', 'RoomsController@roomDetail');
});
Route::resource('/cart','CartController');
//Route::get('/booking','RoomController@booking');
Route::resource('room','RoomController');
Route::resource('/booking','BookingController');
Route::get('/booking/create/{id}', 'BookingController@create');
Route::get('/customer/create/{id}', 'CustomersController@create');
Route::get('/customer/index', 'CustomersController@index');
Route::resource('/customer','CustomersController');
Route::resource('/search','SearchController');
Route::get('/search','SearchController@create');
Route::get('/room/room/{id}','RoomController@roomCates');
