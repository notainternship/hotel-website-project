const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Add near top of file
let ImageminPlugin = require( 'imagemin-webpack-plugin' ).default;


mix.webpackConfig( {
    plugins: [
        new ImageminPlugin( {
//            disable: process.env.NODE_ENV !== 'production', // Disable during development
            pngquant: {
                quality: '95-100',
            },
            test: /\.(jpe?g|png|gif|svg)$/i,
        } ),
    ],
} );

mix.js('resources/assets/js/app.js', 'public/js')
    .copy('resources/assets/js/custom.js', 'public/js/custom.js')
    .copy('resources/assets/js/bootstrap.min.js', 'public/js/')
    .copy('resources/assets/js/jquery.min.js', 'public/js/')
    .copy('resources/assets/js/jquery-ui.js', 'public/js/')
    .copy('resources/assets/css/jquery-ui.css', 'public/css/')
    .copy('resources/assets/css/admin-css.css', 'public/css/')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .copy( 'resources/assets/images', 'public/images', false );
mix.copy('resources/assets/index.php', 'public/');
